% iterated logistic eqn - symbol sequence

sn=100000; % # of sequences
sl=3; % length of sequence
x0=.2;
r=4.0;

n=sn*sl;
x=zeros(n,1);
x(1)=x0;
for i=1:n-1
	x(i+1)=r.*x(i).*(1-x(i));
end

s=(2.^(sl-1:-1:0))*reshape(x<.5,sl,sn);
hist(s,2^sl);
