function f=lotka()
	f=@(x)[x(1)-.25*x(1)*x(2); .2*x(1)*x(2)-.6*x(2)];
end
