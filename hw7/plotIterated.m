function plotIterated(f,x0,nSkip=0,nPlot=100)
	for i=1:nSkip
		x0=f(x0);
	end
	x=zeros(2,nPlot);
	x(:,1)=x0;
	for i=1:nPlot-1
		x(:,i+1)=f(x(:,i));
	end

	figure;
	subplot(2,1,1);
	plot(0:nPlot-1,x(1,:));
	subplot(2,1,2);
	plot(0:nPlot-1,x(2,:));

	figure;
	hold on;
	nEnd=floor(nPlot/10);
	if nEnd<10
		plot(x(1,:),x(2,:),'--');
	else
		r=1:nPlot-nEnd;
		plot(x(1,r),x(2,r),'--');
		r=nPlot-nEnd+1:nPlot;
		plot(x(1,r),x(2,r),'.r');
	end
end
