function plotDifferential3(f,x0,tEnd=100)
	[t,x]=ode45(@(t,x)(f(x)),[0 tEnd],x0);
	x=x';

	figure;
	subplot(3,1,1);
	plot(t,x(1,:));
	subplot(3,1,2);
	plot(t,x(2,:));
	subplot(3,1,3);
	plot(t,x(3,:));

	figure;
	plot3(x(1,:),x(2,:),x(3,:));
end
