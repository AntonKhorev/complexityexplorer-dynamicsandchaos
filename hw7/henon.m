function f=henon(a,b)
	f=@(x)[x(2)+1-a*x(1)^2;b*x(1)];
end
