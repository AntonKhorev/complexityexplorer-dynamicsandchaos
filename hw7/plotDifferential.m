function plotDifferential(f,x0,tEnd=100)
	[t,x]=ode45(@(t,x)(f(x)),[0 tEnd],x0);
	x=x';

	figure;
	subplot(2,1,1);
	plot(t,x(1,:));
	subplot(2,1,2);
	plot(t,x(2,:));

	figure;
	plot(x(1,:),x(2,:));
end
