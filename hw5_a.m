function hw5_a(fn)
	nSkips=100;
	nPlots=200;
	nRSteps=2000;

	% r = row vector; x = scalar or col vector
	x=0.3; % seed
	if fn==1
		r=linspace(0,4,nRSteps);
		f=@(x)(r.*x.*(1-x));
	elseif fn==2
		r=linspace(5,7,nRSteps);
		f=@(x)(r.*x.^2.*(1-x));
	elseif fn==3
		r=linspace(0,5,nRSteps);
		f=@(x)(r.*sin(pi/2*x));
	else
		error('invalid fn')
	end

	figure;
	hold on;
	for i=1:(nSkips+nPlots)
		x=f(x);
		if i>nSkips
			plot(r,x,'.',"markersize",1);
		end
	end
end
