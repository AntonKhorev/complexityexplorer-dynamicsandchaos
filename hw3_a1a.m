% compare two iterated logistic eqns

n=40;
x0=[.2; .20001];
r=4.0;

x=zeros(length(x0),n);
x(:,1)=x0;
for i=1:n-1
	x(:,i+1)=r.*x(:,i).*(1-x(:,i));
end

subplot(2,1,1);
plot(x');
subplot(2,1,2);
plot(diff(x));
