clear all; close all;

T0=5;
dT_T=@(T)(0.2*(20-T));
T_t=@(t)(20-15*exp(-0.2*t));

t0=0;
t1=20;

figure;
t=t0:.1:t1;
plot(t,T_t(t),'0');
hold on;
dt=[2 1 .5 .2 .1];
for i=1:length(dt)
	t=t0:dt(i):t1;
	T=t*0;
	T(1)=T0;
	for j=1:length(t)-1
		T(j+1)=T(j)+dt(i)*dT_T(T(j));
	end
	plot(t,T,num2str(i));
end
